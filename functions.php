<?php
require_once 'liep/liep_function.php';
// fix qtranslate - line 788
	// content not available in requested language (bad!!) what now?
//	if(!$show_available){
//		// check if content is available in default language, if not return first language found. (prevent empty result)
//		if($lang!=$q_config['default_language'])
//			return /*"(".$q_config['language_name'][$q_config['default_language']].") ".*/qtrans_use($q_config['default_language'], $text, $show_available);
//		foreach($content as $language => $lang_text) {
//			$lang_text = trim($lang_text);
//			if(!empty($lang_text)) {
//				return "(".$q_config['language_name'][$language].") ".$lang_text;
//			}
//		}
//	}


// change "and" to "&" in the_title() finction
function fix_and($title) {
  $match = '/ and /';
  $replacement = ' &amp; ';

  $title = preg_replace($match, $replacement, $title);
  return $title;
}
add_filter( 'the_title', 'fix_and', 10);

define('THEME_TEMPLATE_URL', get_bloginfo('template_directory'));

// Add specific CSS class by filter
add_filter('body_class','scs_class_names');
function scs_class_names($classes) {
	global $post;
	
	$output = get_post_meta($post->ID, 'body_class', $single = true);
	if($output !== '') {
		$classes[] = $output;
		return $classes;
	}
	return $classes;
}

// set gravity forms minimum year
add_filter("gform_date_min_year", "set_min_year");

// outputs custom field or nothing
function echo_custom_field( $cf, $def = '', $echo = 0 ){
	$val = custom_field($cf, NULL, NULL, false);
	if($echo) return ($val ? $val : $def); 
	else echo ($val ? $val : $def);
}
function custom_field($cf, $cs, $th, $kp=false){
    return;
}
function set_min_year($min_year){
    return 2011;
}

// add a class to the body tag if you're in a program area section
function my_body_class() {
	global $post;
	
	$p = get_post_ancestors($post->ID);
	#echo '<script type="text/javascript">console.log("'.basename(get_permalink($p[0])).'");< /script>';

	if($p[0] == 4432) {
		return basename(get_permalink($p[1]));
	} elseif ($p[0] == 4438 || $post->ID == 4438) {
		return basename(get_permalink(4438));	
	}
	return FALSE;
}

function child_of($id=NULL) { // deprecate??
	global $post;
	$p = get_post_ancestors($post->ID);
	if (in_array($id, $p) || $post->ID == $id) { return TRUE; } else { return FALSE; }
}

function is_child($pageID) { 
	global $post; 
	if( is_page() && ($post->post_parent==$pageID || ($post->ancestors && in_array( $pageID, $post->ancestors) )) ) return true;
	else return false;
}

// checks if category is within another
function is_cat_child($child,$parent){
	if(gettype($child)==='string')$child=get_cat_id($child);
	if(gettype($parent)==='string')$parent=get_cat_id($parent);
	return cat_is_ancestor_of($parent,$child);
}

// create pagination for custom taxonomies
function custom_tax_pagination( $curr_page, $max_pages, $num_pages_to_display, $taxo, $term, $extra, $extra_position = 2 ){
	if( $max_pages > 1 ):
		echo '<div class="navigation custom-paged">';
		#if( $extra_position == 0 ) DO_NOT_SHOW( $extra );
		if( $extra_position == 1 ) echo $extra;
		$display_pages = ($num_pages_to_display*1)/2;
		$num_start = $curr_page - $display_pages; 
		$num_end = $curr_page + $display_pages;
		if($num_end > $max_pages) $lastpage = $max_pages; else $lastpage = $num_end;
		if($num_start < 1) $i = 0; else $i = $num_start-1;
		if($i > 0) echo '<li><a href="http://staging.learn.utoronto.ca/'.$taxo.'/'.$term.'/page/1" title="Go to first page">&lt;</a></li><li><a href="http://staging.learn.utoronto.ca/'.$taxo.'/'.$term.'/page/'.($curr_page-1).'">prev</a></li><li>...</li>';
		while( $i<$lastpage ) { 
			++$i;
			if($i == $curr_page) echo '<li><strong>'.($i).'</strong></li>'; 
			else echo '<li data-num=""><a href="http://staging.learn.utoronto.ca/'.$taxo.'/'.$term.'/page/'.($i).'">'.($i).'</a></li>'; 
		}
		if($lastpage < $max_pages) echo '<li>...</li><li><a href="http://staging.learn.utoronto.ca/'.$taxo.'/'.$term.'/page/'.($curr_page+1).'">next</a></li><li><a href="http://staging.learn.utoronto.ca/'.$taxo.'/'.$term.'/page/'.($max_pages).'" title="Go to last page">&gt;</a></li>';
		if( $extra_position == 2 ) echo $extra;
		echo '<div class="clearfix"></div>';
		echo '<div>';
	endif;
}


/* news plus functions */
function map_single_category_template ($the_template){
	foreach( (array) get_the_category() as $cat ) {
		if($cat->category_parent!==0)$parent = get_category($cat->category_parent);		
		if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") ) return TEMPLATEPATH . "/single-{$cat->slug}.php";		
		elseif($parent && $parent->slug && file_exists(TEMPLATEPATH . "/single-{$parent->slug}.php") ) return TEMPLATEPATH . "/single-{$parent->slug}.php";
	}	
	return $the_template;
}
add_filter('single_template', 'map_single_category_template');

function the_news_category( $delim, $class, $asString, $echo = true ){
	$cats = array();
	foreach(get_the_category() as $category) {
		if($category->cat_name === 'News+')
			$cats[] = ($asString)?$category->slug:'<a class="category '. $category->slug .' '.$class.'" href="'. get_category_link( $category->term_id ) .'" title="'. sprintf( __( "View all posts in %s" ), 'All News' ) .'" '. '>All News</a> ';
		else 
			$cats[] = ($asString)?$category->slug:'<a class="category '. $category->slug .' '.$class.'" href="'. get_category_link( $category->term_id ) .'" title="'. sprintf( __( "View all posts in %s" ), $category->name ) .'" '. '>'. $category->name .'</a>';
	}
	$cat_list = implode($delim, $cats);
	if($echo) echo $cat_list;
	else return $cat_list;
}

function the_news_tags( $delim = ', ', $list = false ){
	$tags = get_the_tags();
	if($list) foreach($tags as $tag){ echo "<li>$tag->name</li>"; }
	else { $arr = array_map(function($o){return $o->name;},$tags); echo implode($delim,$arr); }
}

define('THEME_TEMPLATE_URL', get_bloginfo('template_directory'));
function news_template_select() {
    if (is_category() && !is_feed()) {
        if (is_category(get_cat_id('news')) || cat_is_ancestor_of(get_cat_id('news'), get_query_var('cat'))) {
            load_template(TEMPLATEPATH . '/category-news.php');
            exit;
        }
    }
}
add_action('template_redirect', 'news_template_select');

add_image_size( 'news-thumb-page', 236, 531, true );
add_image_size( 'news-thumb-mini', 145, 135, true );
add_image_size( 'news-thumb-home', 232, 107, true );



// ADDS AUTOMATIC FEED LINKS
add_theme_support( 'automatic-feed-links' );


# -------------------------------------------------------------------------------------------------------------------------
# create for-use-in-editor shortcodes

// link to microsite
function microsite_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'id' => '0'
	), $atts ) );

	return "<br /><hr /><p><strong><a class='dark_gray' href='http://learningtochangelives.ca' target='_blank'>Click to view personal perspectives for the School's students and instructors.</a></strong></p>";
}
add_shortcode( 'microsite', 'microsite_shortcode' );

// back to top link shortcode
function back_to_top_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'border' => '1',
		'bold' => '1',
		'padding' => '1'
	), $atts ) );
	
	$style = "";
	if($border) $style .= "border-top:1px solid #ccc;"; 
	if($padding) $style .= "padding: 10px 10px 15px;"; 
	if($bold) $style .= "font-weight: bold;"; 

	return '<div style="text-align:right;'.$style.'"><a href="#top">Back to Top</a></div>';
}
add_shortcode( 'back_to_top', 'back_to_top_shortcode' );


// SCS Cabinet Accordian Collection shortcode
function add_cabinet_accordian( $atts, $content = null ) {
	extract( shortcode_atts( array( 
		'type' => 1,
		'tag' => 'div',
		'class' => '',
		'whitespace' => 1,
		'openclose' => 0
	), $atts ));	
	if(!!$openclose) $class .= ' exclusive';
	$selector = ( $type == 1 || $type == 'single' ? "id=\"cabinet\" class=\"{$class}\"" : "class=\"cabinet-set {$class}\"" );
	$contents = do_shortcode($content);
	if($whitespace) $contents = str_replace('<br />',"",$contents);
	return "<{$tag} {$selector}>" . $contents . "</{$tag}>";	
}
// SCS Cabinet Item shortcode
function add_cabinet_item( $atts, $content = null ) {
	extract( shortcode_atts( array( 
		'title' => '',
		'tag' => 'a',
		'wrapper' => 'div',
		'wrapper_class' => '',
		'title_class' => '',
		'class' => '',
		'open' => 0,
		'hashTag' => ''
	), $atts ));	
	
	$handle = "<{$tag} class=\"cabinet-handle\">" . $title . "</{$tag}>";
	if(!empty($open)) $class .= ' top-open';
	$contents = do_shortcode($content);
	
	if($tag == 'a') $handle = "<{$tag} class=\"cabinet-handle {$title_class}\" href=\"#{$hashTag}\">{$title}</{$tag}>";
	else $handle = "<{$tag} class=\"cabinet-handle {$title_class}\">{$title}</{$tag}>";
	$drawer = "<div class=\"cabinet-drawer {$class}\">{$contents}<p>&nbsp;</p><p></p></div>";
	$item = $handle . $drawer;
	if(!empty($wrapper)) $item = "<{$wrapper} class=\"{$wrapper_class}\">{$item}</{$wrapper}>";
	return $item;	
}
// SCS Cabinet Button shortcode
function add_cabinet_button( $atts, $content = null ) {
	extract( shortcode_atts( array( 
		'tag' => 'a',
		'type' => null,
		'href' => '#',
		'class' => ''
	), $atts ));	
	if(empty($type) || !stristr('close|next|prev',$type)) return;
	if(empty($content)) $content = ucwords($type);
	if(empty($class)) $class = 'cabinet-button';
	$href .= $type;	
	if($tag == 'a') $button = "<{$tag} class=\"cabinet-{$type} {$class}\" href=\"{$href}\">" . $content . "</{$tag}>";
	else $button = "<{$tag} class=\"cabinet-{$type} {$class}\">" . $content . "</{$tag}>";
	return $button;	
}
add_shortcode( 'cabinet', 'add_cabinet_accordian' );
add_shortcode( 'cabinet_drawer', 'add_cabinet_item' );
add_shortcode( 'cabinet_button', 'add_cabinet_button' );

// LIEP application checklist shortcode
function liep_checklist_shortcode( $atts ) {
	return '<script type="text/javascript">' .
'jQuery(document).ready(function(){' .
	'var checkboxes = jQuery("input","#liep-app-checklist");' .
	'checkboxes.change(function(){' .
		'var checked = checkboxes.filter(":checked").length,' .
			'length = checkboxes.length,' .
			'readout = checked + " of " + length + " items checked. ";' .	
			'complete = ( checked == length ? "Checklist has been completed." : "Please complete the checklist." );' .
		'jQuery("#liep-app-checklist-readout").html( readout + complete );' .
	'});' .
'});' .
'</script>' .
'';
}
add_shortcode( 'liep_checklist', 'liep_checklist_shortcode' );


// SCS Sideblock shortcode
function add_sideblock( $atts, $content = null ) {
	extract( shortcode_atts( array( 
		'title' => 'Important Course Information',
		'width' => '35%',
		'href' => '#',
		'class' => ''
	), $atts ));	
	$contents = do_shortcode($content);
	$style = ( $width == '35%' ? '' : " style=\"width:{$width};\"" );
	$output = "<div class=\"sideblock\"{$style}><h2>{$title}</h2>{$contents}</div>";
	return $output;	
}
add_shortcode( 'sideblock', 'add_sideblock' );

// SCS Clearfix shortcode
function add_clearfix( $atts ) {
	extract( shortcode_atts( array( 
		'tag' => 'div'
	), $atts ));	
	$output = "<{$tag} class=\"clearfix\"></{$tag}>";
	return $output;	
}
add_shortcode( 'clearfix', 'add_clearfix' );


// CES Forms shortcode / For CES General and FSWP Applications
function ces_forms_shortcode( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'id' => '',
		'name' => '',
		'overridename' => '',
		'showversion' => false,
		'textonly' => ''
	), $atts ) );
	
	if(empty($id)) return false;
	$options = get_option('aboutus_options'); $cesoptions = $options['ces_forms']; $cesformpath = $cesoptions[$id];
	
	$cestitle = $cesoptions[$id.'_name']; $cesversion = $cesoptions[$id.'_version'];
	if(!empty( $overridename )) $cestitle = $overridename; elseif(empty( $cestitle )) $cestitle = $name;
	if(empty( $content )): $ceslinktext = $cestitle;
	else:
		$ceslinktext = str_replace('%vs%',$cesversion,$content,$vs);
		$ceslinktext = str_replace('%form%',$cestitle,$ceslinktext,$num);
		if($num<1)$ceslinktext = $cestitle;
	endif;
	
	$addon = ( !!$versionDisplay ? ' <span>'.(str_replace('%vs%',$cesversion,$versionDisplay,$vs)).'</span>' : '' );	
	
	if(empty($showversion) || empty($cesversion)) $addon = '';
	else $addon = ' <span>[ '.$cesversion.' ]</span>';
	
	if(empty($ceslinktext)) return false;		
	elseif(empty($cesformpath)) return '<span>'.$ceslinktext.'</span>'.$addon; 
	elseif(!empty($textonly) ) return '<span style="font-style:italic;">'.$ceslinktext.'</span>'.$addon; 
	else return '<a href="'.$cesformpath.'" target="_blank" onclick="trackOutboundLink(this, \'CES Form Links\', \''.$id.'\', \''.$cesoptions[$id.'_name'].' '.$cesoptions[$id.'_version'].'\', true );">'.$ceslinktext.'</a>'.$addon;
}
add_shortcode( 'ces_form', 'ces_forms_shortcode' );

// SCS Forms shortcode / for Awards and Bursaries
function scs_forms_shortcode( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'id' => '',
		'name' => '',
		'overridename' => '',
		'textonly' => ''
	), $atts ) );
	
	$addon = '';
	if(empty($id)) return false;
	$options = get_option('aboutus_options'); $scsoptions = $options['scs_forms']; $scsformpath = $scsoptions[$id]; $scsformhtml = $scsoptions[$id.'_html']; $scsformshow = $scsoptions[$id.'_chbx'];
	if(empty($scsformpath)) $addon = '<strong> – Not accepting applications at this time</strong>';
	if(empty($scsformhtml)) $scsformhtml = ''; else $scsformhtml = "\n" . $scsformhtml;
	
	$scstitle = $scsoptions[$id.'_name']; 
	if(!empty( $overridename )) $scstitle = $overridename; elseif(empty( $scstitle )) $cestitle = $name;
	if(empty( $content )): $scslinktext = $scstitle;
	else:
		$scslinktext = str_replace('%form%',$scstitle,$scslinktext,$num);
		if($num<1)$scslinktext = $scstitle;
	endif;
	
	if(empty($scslinktext)) return false;		
	elseif(empty($scsformpath)) return '<span class="scs_form nopath" style="font-style:italic;">'.$scslinktext.'</span>'.$addon.$scsformhtml; 
	elseif(!empty($textonly) || empty($scsformshow)) return '<span class="scs_form" style="font-style:italic;">'.$scslinktext.'</span>'.$addon.$scsformhtml; 
	else return '<a class="scs_form" href="'.$scsformpath.'" target="_blank">'.$scslinktext.'</a>'.$addon.$scsformhtml;
}
add_shortcode( 'scs_form', 'scs_forms_shortcode' );

// Admin only content - only visible to registered admin users
function scs_admin_only_content( $content = '' ){
    //if(is_admin()){ return $content; } else return '';
	return is_admin();
}
add_shortcode( 'adminonly', 'scs_admin_only_content' );


// Google Maps hack shortcode
function google_maps_hack_shortcode( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'height' => '350',
		'width' => '600',
	), $atts ) );
	
	$content2 = str_replace( "600", "980" ,$content );
	$content2 = str_replace( "350", "670" ,$content2 );
	
	$src_end = '&f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;&hl=en&ll=43.667344,-79.395833&output=embed';
	$src_end = "";
	
	return '<iframe src="'.$content.'" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" width="'.$width.'" height="'.$height.'"></iframe><small><a style="color: #0000ff; text-align: left;" href="'.$content2.$src_end.'" target="_blank">View Larger Map</a></small>';
}
add_shortcode( 'gmaps', 'google_maps_hack_shortcode' );

function display_shortcodes_shortcode($content = null){
	global $shortcode_tags; $out = "";
	$out.= "<pre>"; $out .= print_r($shortcode_tags, false); $out.= "</pre>";	
}
add_shortcode( 'all_shortcodes', 'display_shortcodes_shortcode' );

// SCS Notice Display shortcode (disappears after date_due, shows only after date_start)
function scs_notices_shortcode( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'id' => '1',
		'bgcolor' => '#ffc',
		'name' => '',
		'border' => 1,
		'debug' => false
	), $atts ) );
	
	$due = get_post_meta($id, 'date_due', true);
	$ddate = strtotime( $due );
	$start = get_post_meta($id, 'date_start', true);
	$sdate = strtotime( $start );
	$data_dump = '';
	$cdate = time();
	$show_notice = false;
	
	if( !empty($due) && $ddate > $cdate ) $show_notice = true; 
	if( !empty($start) && $sdate < $cdate ) $show_notice = true; 
	$valid = ( $show_notice ? 'show' : 'hide' );
	
	if(!!$debug) $data_dump = '" data-start="'.$sdate.'" data-expires="'.$ddate.'" data-now="'.$cdate.'"" data-show="'.$valid;
	
	if( $show_notice ) {	
		if(empty($content)) :
			$npost = get_post($id);
			$content = $npost->post_content;	
		endif;	
		return '<table data-name="'.$name.$data_dump.'" style="border-width: '.$border.'px"><tbody><tr><td style="background-color: '.$bgcolor.'">'.$content.'</td></tr></tbody></table>';
	}
}
add_shortcode( 'scs_notice', 'scs_notices_shortcode' );


// SCS Page Content Include shortcode
function scs_page_content_include( $atts, $content = '' ) {
	extract( shortcode_atts( array( 'id' => 1, 'before' => '', 'after' => '' ), $atts ));	
	if(empty($before) || empty($after)) $before = $after = '';	
	$page = get_post($id); $article = $page->post_content;	
	$admin = ( current_user_can('edit_posts') ? '<p>[ <a href="http://learn.utoronto.ca/wp-admin/post.php?post='.$id.'&action=edit" style="font-variant:small-caps; color:#E69403;">edit</a> ] <span style="font-size:80%;">' . $id . '</span></p>' : '' );
	return $before . $admin . $article . $content . $after;	
}
add_shortcode( 'pagecontent', 'scs_page_content_include' );


// SCS Media Gallery Item shortcode
function add_scs_media_item( $atts, $content = '' ) {
	extract( shortcode_atts( array( 'link' => '', 'imgsrc' => '', 'type' => 'video', 'class' => '', 'feature' => 0 ), $atts ));
	$img = "<img src='$imgsrc' />";
	$class .= ' iframe' . (!!$feature ? ' feature' : '');
	if(!empty($link))$media_link = "<a href='$link' title='$content' data-type='$type' class='$class'>$img</a>";
	else $media_link = "<span title='$content' data-type='$type' class='$class'>$img</span>";
	return $media_link;	
}
add_shortcode( 'media_item', 'add_scs_media_item' );

// SCS Media Gallery Collection shortcode
function add_scs_media_container( $atts, $content = null ) {
	extract( shortcode_atts( array( 'divider' => 1, 'type' => 'vimeo' ), $atts ));
	$class = 'scs-media-container' . (!!$type ? ' '.$type : '');
	$opener = "<div class='$class'>";
	$closer = ( $divider ? "<hr /></div><br />" : "<div style='clear:both;'></div></div>" );
	$contents = do_shortcode($content);
	$contents = str_replace('<br />',"",$contents);
	return $opener . $contents . $closer;	
}
add_shortcode( 'media_container', 'add_scs_media_container' );

// SCS jQuery cycle shortcode
function add_cycle_slider( $atts, $content = null ) {
	extract( shortcode_atts( array( 'selector' => '', 'fx' => '', 'timeout' => 10000, 'speed' => 300, 'autoheight' => '','pause' => 'true', 'prev' => '', 'next' => '', 'pager' => '', 'numbers' => 0 ), $atts ));
	$js = "";
	$js .= "jQuery('$selector > ul').cycle({";
	$js .= " timeout : $timeout,";
	if($pause) $js .= " pause : $pause,";
	if($fx) $js .= " fx : '$fx',";
	if($prev) $js .= " prev : '$prev',";
	if($next) $js .= " next : '$next',";
	if($pager) $js .= " pager : '$pager',";
	if(!empty($autoheight)) $js .= " before : onAfter,";
	$js .= " speed : $speed ";
	$js .= "});";
	if($numbers == 0) $js .= "jQuery('$selector $pager a').html('.');";
	elseif($numbers == 2) $js .= "jQuery('$selector $pager a').html(function(){".
		"var me=jQuery(this),i=me.index('$selector $pager a'),o=jQuery('$selector > ul > li').eq(i).find('.header');return o.text();".
	"});";
	
	$ifnojs = "if(!jQuery['cycle']){ ";
	$ifnojs .= " var xmybcd612 = document.createElement('srcipt'); xmybcd612.src=''; xmybcd612.type='text/javascript';  ";
	$ifnojs .= " }";
	
	$auto = "function onAfter(curr, next, opts, fwd){".
		"var index = opts.currSlide;".
		"var ht = jQuery(this).height(), min = jQuery('$pager').height();".
		"jQuery(this).parent().animate({height: (ht>min?ht+20:min)});".
	"}";
	
	return "<script type=\"text/javascript\">jQuery(document).ready(function(){{$auto}; {$ifnojs}; {$js}; });</script>";
}
add_shortcode( 'jcycle', 'add_cycle_slider' );


# -------------------------------------------------------------------------------------------------------------------------
# create UTM theme options 

// UTM Course Icon Legend
function add_certificate_course_legend( $content = null ){
	return '<div class="iconlist"><p>Available Format:</p><div class="icon c"></div><p>In-Class</p><div class="icon o"></div><p>Online</p><div class="icon x"></div><p>In-class or Online</p><div class="icon h"></div><p>Hybrid</p><div class="icon n"></div><p>Not at UTM</p></div><div class="clearleft"><hr /></div>';
}
add_shortcode( 'utm_course_legend', 'add_certificate_course_legend' );

// UTM Course Icon Grouping
function add_certificate_courses( $atts, $content = null ){
		$wrap_start = '<div id="courselist" class="certificate_courselist"><p></p>';$wrap_end='</div><div class="clearleft"></div>';//}
	return $wrap_start . do_shortcode( $content ) . $wrap_end;
}
add_shortcode( 'utm_course_highlight', 'add_certificate_courses' );

// UTM Course Icon Grouping
function add_utm_icon( $atts, $content = null ){
	extract( shortcode_atts( array( 'type' => 'n' ), $atts ));
	return "<div class=\"icon $type\"></div>";
}
add_shortcode( 'utm_icon', 'add_utm_icon' );

// UTM Course Special Icons
function add_utm_sicon( $atts, $content = null ){
	extract( shortcode_atts( array( 'type' => 'n' ), $atts ));
	$front = "<span class=\"utm-display\"> <sup>";
	$back = "</sup></span>";
	switch($type){
		case "c": return $front."*".$back;
		case "o": return $front."&#134;".$back; //†
		case "x": return $front."&#167;".$back; //§
		case "h": return $front."&#182;".$back; //¶
		default: return $front."".$back; /* also for "n" */
	}
}
add_shortcode( 'utm_sicon', 'add_utm_sicon' );

// UTM Course Special Icons
function add_utm_slegend( $content = null ){
	$front = "<hr /><div class=\"utm-display\"><p style='margin:0;'>"; $back = "</p></div>";
	return $front."<strong>Notes</strong>:<em> <sup>*</sup> Inclass,  <sup>&#134;</sup> Online,   <sup>&#167;</sup> Inclass or Online,   <sup>&#182;</sup> Hybrid</em>".$back;
}
add_shortcode( 'utm_slegend', 'add_utm_slegend' );

#
# -------------------------------------------------------------------------------------------------------------------------



//*
// SCS Destiny Public View Links shortcode
function destiny_public_view_shortcode( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'id' => '',
		'title' => false,
		'list' => false,
		'active' => '1',
		'code' => null,
		'icon' => null,
		'follow' => null,
		'class' => ''
	), $atts ) );
	
	$attr = ($active=='1'?'':'x');
	if(empty($id)) $class .= ' destiny inactive';
	
	$output = 
	'<a '.$attr.(empty($id)?'':'href="http://2learn.utoronto.ca/uoft/search/publicCourseSearchDetails.do?method=load&amp;cms=true&amp;courseId='.$id.'"') .
	(!empty($title) ? ' title="Click here for more information about '.$content.'."' : '') .
	' onClick="trackOutboundLink(this, \'Public View Links\', \'visit\', \''.$id.' - '.$content.'\'); return false;" class="' . $class . '"' .
	'>'.(!empty($code) ? 'SCS '.$code.' ' : '').$content.'</a>';
	
	if($follow) $output.=" ".$follow;
	if($icon) $output="<div class=\"icon $icon\"></div>".(empty($list)?"<p>".$output."</p>":$output);
	
	if(!empty($list)) $output = '<li>'.$output.'</li>'; 
	return $output;
}
add_shortcode( 'public_view', 'destiny_public_view_shortcode' );
//add_shortcode( 'dpv', 'destiny_public_view_shortcode' );

// SCS Instructor Biography shortcode
function get_intructor_bio( $atts ){
	extract( shortcode_atts( array('name' => '','info' => 1), $atts ) );	
	if(!empty($name)){
		$page = get_posts(
			array(

				'name'      => $name,
				'post_type' => 'instructor_bios'
			)
		);
		if(!empty($page)){
			$title = explode(', ',$page[0]->post_title);
			$fname = $title[1]; $lname = $title[0];
			$slug = str_replace('-','.',sanitize_title("$fname $lname") );
			$description = $page[0]->post_content;
			$meta = get_post_meta($page[0]->ID);
			$creds = $meta['bio_credentials'][0];
			$courses = nl2br($meta['bio_courses'][0]);			
			$courses = do_shortcode($courses);		
			$output = "<!--------------------------------------------------><a name=\"$slug\"></a>";
			if($info) $output.="<div class=\"instructor snug\"><strong>$fname $lname</strong>, <span class=\"credentials\">$creds</span><p></p><div class=\"biography\">"; else $output.='<p>';
			$output.=$description;
			if($info) $output.="</div>$courses</div>"; else $output.='</p>';
			
			return $output;
		}		
		return '<span style="display:none;" class="no-page">There is no such page.</span>';#'<h3 style="background-color:blue;">gooooo</h3>';
	}
	return '<span style="display:none;" class="no-name">The name slug is missing.</span>';#'<h3 style="background-color:red;">beeeee</h3>';
}
add_shortcode( 'instructor', 'get_intructor_bio' );

# -------------------------------------------------------------------------------------------------------------------------
# create editor buttons


//Adds a button to the visual editor to insert the 'public_view' shortcode
function add_public_view_button() { 
   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   } 
   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'add_public_view_tinymce_plugin' );
      add_filter( 'mce_buttons', 'register_public_view_button' );
   } 
}
add_action('init', 'add_public_view_button');

function add_public_view_tinymce_plugin( $plugin_array ) {
   $plugin_array['publicview'] = get_template_directory_uri() . '/_js/public_view.js';
   return $plugin_array;
}
function register_public_view_button( $buttons ) {
   array_push( $buttons, "|", "publicview" );
   return $buttons;
}

# -------------------------------------------------------------------------------------------------------------------------

//add a button to the content editor, next to the media button
//this button will show a popup that contains inline contentx
//add_action('media_buttons_context', 'add_instructor_bio_button');

//add some content to the bottom of the page 
//This will be shown in the inline modal
add_action('admin_footer', 'add_bio_inline_popup_content');

//action to add a custom button to the content editor
function add_instructor_bio_button($context) {
  
  //path to my icon
  $img = 'http://learn.utoronto.ca/wp-content/uploads/2012/11/bio-button.png';
  
  //the id of the container I want to show in the popup
  $container_id = 'popup_container';
  
  //our popup's title
  $title = 'Insert Instructor Biography';

  //append the icon
  $context .= "<a class='thickbox' title='{$title}' href='#TB_inline?width=640&inlineId={$container_id}'><img src='{$img}' /></a>";
  #$context .= "<a class='thickbox' title='{$title}' href='" . get_template_directory_uri() . "/_js/instructor_bio2.php?post_id=236050&TB_iframe=1&width=640&height=447'><img src='{$img}' /></a>";
  
  return $context;
}

# -------------------------------------------------------------------------------------------------------------------------
# edit instructor_bios post type admin view

add_action( 'restrict_manage_posts', 'restrict_manage_instructors' );
function restrict_manage_instructors() {
    global $typenow;
    $taxonomy = 'program_areas'; // Change this
    if( $typenow != "page" && $typenow != "post" ){
        $filters = array($taxonomy);
        foreach ($filters as $tax_slug) {
            $tax_obj = get_taxonomy($tax_slug);
            $tax_name = $tax_obj->labels->name;
            $terms = get_terms($tax_slug);
            echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";

            echo "<option value=''>Show All $tax_name</option>";
            foreach ($terms as $term) { 
                $label = (isset($_GET[$tax_slug])) ? $_GET[$tax_slug] : ''; // Fix
                echo '<option value='. $term->slug, $label == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
            }
            echo "</select>";
        }
    }
}
add_filter('manage_edit-instructor_bios_columns', 'add_program_area_columns');

function add_program_area_columns($new){
	$new = array (
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Title' ),
		'name' => __( 'Instructor Name' ),
		'date' => __( 'Date' ),
		'program-area' => __( 'Program Area' ),
		'code' => __( 'Code' ),
		'author' => __( 'Author' ),
		'slug' => __( 'Slug' ),
	);
	return $new;
}
add_action("manage_posts_custom_column", 'program_area_custom_columns', 10 , 2 );

function program_area_custom_columns($column,$id) {
    global $wpdb, $post;
        switch ($column) {
        case 'program-area':
			echo get_the_term_list($post->ID,'program_areas','',', ','');
            break;
        case 'slug':
            $text = basename(get_post_permalink($id));
            echo $text;
            break;
        case 'code':
            $text = '[instructor name="'.basename(get_post_permalink($id)).'"]';
            echo $text;
            break;
        case 'name':
            $fullname = get_the_title($post->ID,'program_areas','',', ','');
			$nameparts = explode(', ',$fullname);
			$surname = $nameparts[0];
			$fullname = $nameparts[1].' '.$nameparts[0]; 
			$nameparts = explode(' ',$nameparts[1]);
			$givenname = $nameparts[0];
			echo "$givenname $surname";
            break;
        default:
            break;
        } // end switch
}


# -------------------------------------------------------------------------------------------------------------------------
/* Editor Button for Adding Instructor Bios */ 
/* --

	_ Depricated as of 2013-08-24 (Aug 24, 2013) _
	_ Replaced by new Instructor Bio framework _

-- */ 
function add_bio_inline_popup_content() {
?>
n
<div id="popup_container" style="display:none;">
<div class="wrap" id="instructor_bio_form">
    <div style="padding:15px 15px 0 15px;border-bottom:1px solid #ccc;padding-bottom:10px;">
    	<h3 style="color:#5A5A5A!important; font-family:Georgia,Times New Roman,Times,serif!important; font-size:1.8em!important; font-weight:normal!important;">Insert an Instructor Bio</h3>
    	<span>Input the bio information below.</span>
    </div>
    <div style="padding:15px 15px 0 15px;">
        <div id="bio_output" style="display:none;"><pre><code></code></pre></div><form id="instructor_bio_input_form">
        <div id="bio_input">
            <p><label>Instructor Name<span>*</span></label><input type="text" name="instr_name" id="instr_name" placeholder="Paul Brenner" required="required" /><span class="msg">Required.</span></p>
            <p><label>Instructor Credentials</label><input type="text" name="instr_creds" id="instr_creds" placeholder="MD, PhD, MSc" /><span class="msg">Optional (but recommended).</span></p>
            <p><label>Instructor Biography</label><textarea style="width:300px;height:200px;" type="text" name="instr_bio" id="instr_bio" placeholder="Paul Brenner was a very remarkable man..."></textarea></p>
            <div id="courses"></div>
            <p><input type="button" value="Add Course" class="button" onclick="addCourse()" /></p>
            <p><label>Insert into Editor?</label><input type="checkbox" name="insert_first" id="insert_first" checked="checked" /><span class="meta">If unchecked, only HTML will show.</span></p>
        </div>
        <div style="margin-top:30px;padding-top:10px;border-top:1px solid #ccc;width:600px;"><input type="button" value="Insert Bio" id="insert_button" class="button-primary" onclick="InsertBio()" /><input type="reset" value="Clear Fields" class="button" /><input type="button" value="Close Window" class="button" onclick="tb_remove()" /></div></form>
    </div>
</div>
<style type="text/css">
#instructor_bio_form label {
	float: left;
	width: 160px;
	font-size: 14px;
	font-family: sans-serif;
	display: block;
	vertical-align: bottom;
	padding-top: 8px;
}
#instructor_bio_form input[type=text] {
	padding: 5px;
	fonat-family: sans-serif;
	font-size: 12px;
}
#instructor_bio_form input[type=checkbox] {
	margin: 9px 5px 5px;
	*margin: 5px;
}
#instructor_bio_form span.meta {
	vertical-align: middle;
	vertical-align: -webkit-baseline-middle;
	color: #888;
	margin: 10px;
}
#instructor_bio_form span.msg {
	color: #888;
	margin: 10px;
}
#instructor_bio_form span.fade {
	color: #bbb;
}
#instructor_bio_form label span {
	color: #c00;
	margin: 5px;
}
#instructor_bio_form pre {
	white-space: pre-wrap;       /* css-3 */
	white-space: -moz-pre-wrap !important; /* Mozilla, since 1999 */
	white-space: -pre-wrap;      /* Opera 4-6 */
	white-space: -o-pre-wrap;    /* Opera 7 */
	word-wrap: break-word;       /* Internet Explorer 5.5+ */
}
#instructor_bio_form #bio_output {
	border-top:1px solid #ccc;width:600px;margin-bottom:100px;
	padding-bottom: 10px;
}
#instructor_bio_form #bio_output pre {
	margin:10px;
}
#bio_output, #bio_output pre, #bio_output code {
	background-color: #fff;
}</style>
<script type="text/javascript">
function addCourse( id ){
	id = id || jQuery('p','#courses').length;
	id++;
	var tmpl = '<p data-id="'+id+'">'+
        	'<label style="height:60px;">Course #<mark>'+id+'</mark><br/><span class="fade">(ID, Code, Title)</span></label>'+
            '<input type="text" name="course_id[]" id="course_id_'+id+'" placeholder="106599" required="required" />'+
            '<input type="text" name="course_code[]" id="course_code_'+id+'" placeholder="0150" /><br />'+
            '<input style="width:300px;" type="text" name="course_title[]" id="course_title_'+id+'" placeholder="Academic English" required="required" />'+
            '<br /><span class="meta">Shows link description on mouseouver.</span>'+
        '</p>';
	jQuery('#courses').append( tmpl );
	return;
}
function Course( $p ){	
	var id, course_id, course_code, course_title;	
	this._eat = function( food ){
		if( food && food.constructor == jQuery ){
			id = food.attr('data-id');
			course_id = food.find('#course_id_'+id).val(),
			course_code = food.find('#course_code_'+id).val(),
			course_title = food.find('#course_title_'+id).val()
		}
	}	
	this.output = function(){
		if(!!course_id){
			if(!!course_title){
				if(!!course_code){ 
					return '[public_view id="'+course_id+'" code="'+course_code+'" ]'+course_title+'[/public_view]';
				} else {
					return  '[public_view id="'+course_id+'"]'+course_title+'[/public_view]';
				}
			}
			
		} else {
			return '';
		}
	}	
	this._eat($p);
}
function InsertBio() {
	var instr_name = jQuery('#instr_name').val() || "",
		instr_creds = jQuery('#instr_creds').val(),
		instr_bio = jQuery('#instr_bio').val(),
		instr_anchor = instr_name.toLowerCase().replace(" ","."),
		instr_courses = [],
		insertIntoWYSIWYG = jQuery('#insert_first').is(':checked'),
		output = '',
		courselist = jQuery('p','#courses');
		
	for(var i=0, p, c;i<courselist.length;++i){
		p = jQuery(courselist[i]);
		c = new Course( p );
		instr_courses.push(c.output());
	}
	
	if( instr_name && instr_bio ){
		output = '<!--------------------------------------------------><a name="'+instr_anchor+'"></a>'+"\n"+
				'<div class="instructor"><strong>'+instr_name+'</strong>, <span class="credentials">'+instr_creds+'</span>'+"\n"+
				'<div class="biography">'+instr_bio+'</div>'+ "\n"+instr_courses.join("\n")+
				'</div>'+"\n";		
		
		//if( insertIntoWYSIWYG ) 
			window.send_to_editor(output);
			console.log( document.getElementById('instructor_bio_input_form') );
			tb_remove();
		//expandHTMLview( output );
	} else {
		alert("Please enter the Instructor Biography information.");
	}
}
function expandHTMLview( s ){
	jQuery('#bio_input').slideUp(200);
	jQuery('#bio_output').slideDown(350).find('code').text( s );
	jQuery('input[type=button]:last').addClass("button-primary");
	jQuery('input[type=reset],input[type=button]').not(":last").hide();
}</script>
</div>
<?php
}

# -------------------------------------------------------------------------------------------------------------------------
/* ENQUEUE HEADER SCRIPTS */

function add_scripts() {
	if(is_admin()) return;
	wp_enqueue_script('jquery-cookie', THEME_TEMPLATE_URL.'/_js/jquery.cookie.js', array('jquery'), '1.0', TRUE);
	wp_enqueue_script('jquery-cycle', THEME_TEMPLATE_URL.'/_js/jquery.cycle.all.min.js', array('jquery'), '1.0', TRUE);
	wp_enqueue_script('font', THEME_TEMPLATE_URL.'/_js/font-controller.js', array('jquery'), '1.0', TRUE);
	wp_enqueue_script('fancybox', THEME_TEMPLATE_URL.'/_js/jquery.fancybox-1.3.4.min.js', array('jquery'), '1.0', TRUE);
}
add_action('init', 'add_scripts');

# -------------------------------------------------------------------------------------------------------------------------
# create theme sidebars

function scs_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => 'Homepage News',
		'id' => 'home_news',
		'description' => 'The homepage news area',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );

	// Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
	register_sidebar( array(
		'name' => 'Homepage Right Column',
		'id' => 'home_right',
		'description' => 'The right sidebar.',
		'before_widget' => '<div class="sidebar_container">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="sidebar_title">',
		'after_title' => '</h4>',
	) );

	// Area 3, located below Page in the footer. Empty by default.
	register_sidebar( array(
		'name' => 'Footer Columns',
		'id' => 'footer-area',
		'description' => 'The footer.',
		'before_widget' => '<nav class="grid_2">',
		'after_widget' => '</nav>',
		'before_title' => '<h5>',
		'after_title' => '</h5>',
	) );

	// Area 4, located on most pages on left side with the "in This Section Title"
	register_sidebar( array(
		'name' => 'Left Column',
		'id' => 'page_sidebar',
		'description' => 'The "In this Section" area.',
		'before_widget' => '<h3>In This Section</h3><nav>', // add mobile menu icon here
		'after_widget' => '</nav>',
		'before_title' => '',
		'after_title' => '',
	) );

	// Area 4, located on most pages on left side with the "in This Section Title"
	register_sidebar( array(
		'name' => 'Left Posts Column',
		'id' => 'post_sidebar',
		'description' => 'The "In this Section" area.',
		'before_widget' => '', // add mobile menu icon here
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );

	// Area 5, located on most pages on right side. Empty by default.
	register_sidebar( array(
		'name' => 'Right Column',
		'id' => 'normal_sidebar',
		'description' => 'The sidebar on most pages.',
		'before_widget' => '<div class="sidebar_container">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="sidebar_title">',
		'after_title' => '</h4>',
	) );
	
		// business-professionals, 
		register_sidebar( array(
			'name' => 'Business & Proffesional Sidebar',
			'id' => 'business-professionals',
			'description' => 'The sidebar on Business and professional Studies Pages.',
			'before_widget' => '<div class="sidebar_container">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="sidebar_title">',
			'after_title' => '</h4>',
		) );
		
		// creative-writing, 
		register_sidebar( array(
			'name' => 'Creative Writing Sidebar',
			'id' => 'creative-writing',
			'description' => 'The sidebar on most pages.',
			'before_widget' => '<div class="sidebar_container">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="sidebar_title">',
			'after_title' => '</h4>',
		) );
		
		// languages-translation, 
		register_sidebar( array(
			'name' => 'Languages & Translation Sidebar',
			'id' => 'languages-translation',
			'description' => 'The sidebar on L&amp;T pages.',
			'before_widget' => '<div class="sidebar_container">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="sidebar_title">',
			'after_title' => '</h4>',
		) );
		
		// arts-science, 
		register_sidebar( array(
			'name' => 'Arts & Science Sidebar',
			'id' => 'arts-science',
			'description' => 'The sidebar on A&amp;Sc pages.',
			'before_widget' => '<div class="sidebar_container">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="sidebar_title">',
			'after_title' => '</h4>',
		) );
	
		// online-distance-learning
		register_sidebar( array(
			'name' => 'Online Learning Sidebar',
			'id' => 'online-distance-learning',
			'description' => 'The sidebar on OL pages.',
			'before_widget' => '<div class="sidebar_container">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="sidebar_title">',
			'after_title' => '</h4>',
		) );
		
		// international-professionals
		register_sidebar( array(
			'name' => 'International Professionals Sidebar',
			'id' => 'international-professionals',
			'description' => 'The sidebar on IP pages.',
			'before_widget' => '<div class="sidebar_container">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="sidebar_title">',
			'after_title' => '</h4>',
		) );
		
		// international-professionals
		register_sidebar( array(
			'name' => 'Comparative Education Services',
			'id' => 'comparative-education-service',
			'description' => 'The sidebar on CES pages.',
			'before_widget' => '<div class="page_wide sidebar_container">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="sidebar_title">',
			'after_title' => '</h4>',
		) );
	
		// for International Conference for Hybrid Learning.
		register_sidebar( array(
			'name' => 'International Conference for Hybrid Learning',
			'id' => 'ichl',
			'description' => 'The sidebar on IP pages.',
			'before_widget' => '<div class="sidebar_container">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="sidebar_title">',
			'after_title' => '</h4>',
		) );

	// Area 6, located below Page in the footer of the ICHL pages. Empty by default.
	register_sidebar( array(
		'name' => 'ICHL Footer Columns',
		'id' => 'footer-area-ichl',
		'description' => 'The footer.',
		'before_widget' => '<nav class="grid_7">',
		'after_widget' => '</nav>',
		'before_title' => '<h5>',
		'after_title' => '</h5>',
	) );
	
}
/** Register sidebars by running scs_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'scs_widgets_init' );

# -------------------------------------------------------------------------------------------------------------------------
# add posts thumbnails

add_theme_support( 'post-thumbnails' );

# -------------------------------------------------------------------------------------------------------------------------
# add image sizes

add_image_size('search-size', 54, 54, TRUE);

// 
function scs_search_filter($query) {
	$post_type = $_GET['type'];
	if (!$post_type) {
		$post_type = 'any';
	}
    if ($query->is_search) {
        $query->set('post_type', $post_type);
    };
    return $query;
};
add_filter('pre_get_posts','scs_search_filter');

# -------------------------------------------------------------------------------------------------------------------------
# create theme options about

include_once('functions_admin.php');

# -------------------------------------------------------------------------------------------------------------------------
# create theme options ICHL


function scs_login_logo() { ?>
    <style type="text/css">
		body.login div#login { width: 380px; }
        body.login div#login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/_images/scs-logo.png);
            padding-bottom: 30px;
			width:100%;
			background-size: auto;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'scs_login_logo' );