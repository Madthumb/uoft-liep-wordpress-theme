<?php
/**
 * The WordPress Custom Function Class
 *
 * Name:               LIEP Function Reference
 * Description:       A master lists of custom front end function.
 * Version:            1.0.0
 * Author:             Tony Lee
 * Author URI:       http://www.nanomedia.ca
 * Text Domain:     liep_func_ref
 * License:             GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

# ---------------- Database Access ---------------------------------------#

$dbuser        =   'bravaski_liepu';
$dbpassword =   'nBp!N&J7PMCK';
$dbname      =   'bravaski_liep';
$dbhost        =   'localhost';   
$GLOBALS['liep'] = new wpdb($dbuser, $dbpassword, $dbname, $dbhost);  
$GLOBALS['records_pre_page'] = 3;  
define(LIEP_PAGE, 'liep');

#--------------------- Actions and Filters ---------------------------------#

add_action( 'wp_enqueue_scripts', 'liep_theme_styles');
add_action( 'wp_enqueue_scripts', 'liep_theme_scripts' );
add_action( 'init', 'liep_add_page');
add_filter( 'body_class','liep_body_class_names');
add_filter('init', 'liep_query_var');
#--------------------- Functions --------------------------------------------#
/*
 * Function Lists:
 * 
 * 1. get_all_streams()
 * 2. get_stream_id_by_title($title)
 * 3. get_stream_title_by_id($id)
 * 4a. get_participants_by_stream_id_with_rand_limit($id, $limit)
 * 4b. get_participant_by_id($id)
 * 4c. get_participant_by_tags($tags)
 * 4d. get_participants_by_stream_id($id)
 * 5a. get_all_skills_tags()
 * 5b. get_skill_tags_by_ids()
 * 6. get_years_of_experience_array()
 * 7. get_all_skills_tags_by_stream_id($id)
 * 8.get_search_result_of_participant($search_queries)
 * 9a. liep_theme_styles()
 * 9b. liep_theme_scripts()
 * 9c. liep_body_class_names()
 * 10.get_template_part_with_param($name, $param)
 * 11.Ajax Call - get_liep_data($post_data)
 * 12.liep_query_var($qvars)
 * 13.load_jq_ui()
 * 14.formart_braced_data($str)
 */

/**
 * Get All Streams
 * @global type $liep
 * @return type
 */
function get_all_streams(){
    
    global $liep;
 
    $result = $liep->get_results("SELECT * FROM `streams`", ARRAY_A);
    
    return ( empty($result) ? FALSE : $result );  
}
/**
 * Get Stream ID by Title
 * 
 * @global type $liep
 * @param type $title
 * @return type
 */
function get_stream_id_by_title($title){
    
    $title = trim($title);
    if(isset($title)){   
        global $liep;
       $result = $liep->get_row( $liep->prepare("SELECT `id` FROM `streams` WHERE `name` = '%s'", $title), ARRAY_A); 
    }    
    return ( empty($result) ? FALSE : $result );    
   
}
/**
 * Get Stream Title by ID
 * 
 * @global type $liep
 * @param type $id
 * @return type
 */
function get_stream_title_by_id($id){
    if(is_int($id)){    
        global $liep;
        $result = $liep->get_row( $liep->prepare("SELECT `name` FROM `streams` WHERE `id` = '%d'", $id), ARRAY_A);     
    }
    return ( empty($result) ? FALSE : $result );  
}
/**
 * Get participants information by given Stream ID and limition
 * 
 * @global type $liep
 * @param type $id
 * @return type
 */
function get_participants_by_stream_id_with_rand_limit($id, $limit){
    
    if(is_int($id)){  
       global $liep;
       $result = $liep->get_results( $liep->prepare("SELECT * FROM `participants` WHERE `active` = 'yes'  AND `stream_id` = '%d' ORDER BY RAND() LIMIT %d ", $id, $limit), ARRAY_A);     
    }
    return ( empty($result) ? FALSE : $result );         
}
/**
 * Get Participant by ID
 * 
 * @global type $liep
 * @param type $id
 * @return type
 */
function get_participant_by_id($id){

    if(is_int($id)){  
        
        global $liep;
        
        $sql =  $liep->prepare("SELECT * FROM `participants` WHERE `active` = 'yes'  AND `id` = '%d' ", $id);
        $result = $liep->get_results($sql, ARRAY_A);     
        
        return ( empty($result) ? FALSE : $result );         
    }
}
/**
 * Get Participant by tags and stream id
 * 
 * @global type $liep
 * @param type $tags
 */
function get_participant_by_tags($stm_id, $tags, $year_level='', $offset=0, $total=0){
    
        global $liep;
        //$total = '';
        
        $sql = get_search_result_of_participant( $tags );        
        if($sql and !empty($year_level) and $year_level != 'all' ){
                $sql .= ' AND `years_of_experience`='.$year_level;
        }
        
        if( $total == 0){
                $total = $liep->get_results( $sql, ARRAY_A);
        }
        $sql .= ' ORDER BY `id` LIMIT '.$offset. ', '.$GLOBALS['records_pre_page'];        
        //echo $sql;
        $result = $liep->get_results( $sql, ARRAY_A);
        
        return array($total, ( empty($result) ? FALSE : $result ) );
}
/**
 * Get participants information by given Stream ID
 * 
 * @global type $liep
 * @param type $id
 * @return type
 */
function get_participants_by_stream_id($id){
    
    if(is_int($id)){  
       global $liep;
       $result = $liep->get_results( $liep->prepare("SELECT * FROM `participants` WHERE `active` = 'yes'  AND `stream_id` = '%d'", $id), ARRAY_A);     
    }
    return ( empty($result) ? FALSE : $result );         
}
/**
 * Get all skills tags
 *  
 * @global type $liep
 * @return type
 */
function get_all_skills_tags(){
    
    global $liep;
    $result = $liep->get_results("SELECT `stream_id`, `value` FROM `p-options` WHERE `name` = 'skills'  GROUP BY `stream_id`, `value`", ARRAY_A);    

    return ( empty($result) ? FALSE : $result );
}
/**
 * Get skill tags by giving skill id(s)
 * 
 * @global type $liep
 * @param type $ids
 * @return type
 */
function get_skill_tags_by_ids($ids){
    
    global $liep;
    $sql = "SELECT `value` FROM `p-options` WHERE `name` = 'skills'  AND `id` IN ( '{$ids}' ) ";
    $result = $liep->get_results($sql, ARRAY_A);    
    return ( empty($result) ? FALSE : $result );      
}
/**
 * Get default years of experience
 * 
 * @return type
 */
function get_years_of_experience_array(){
    return array(   
                    1=>'1-2 years',
                    2=>'3-5 years',
                    3=>'6-9 years',
                    4=>'10+ years'
                 );
}
/**
 * Get participant skills sets
 * 
 * @global type $liep
 * @param type $id
 * @return type
 */
function get_all_skills_tags_by_stream_id($id){

    if(is_int($id)){ 
        global $liep;
        $result = $liep->get_results( $liep->prepare("SELECT `value` FROM `p-options` WHERE `name` = 'skills' AND `stream_id` = '%d' GROUP BY `value`", $id), ARRAY_A);   
    }
    return ( empty($result) ? FALSE : $result );         
}
/**
 * For front end search participant only.
 * 
 * @param type $seach_queries
 * @return boolean
 */
function get_search_result_of_participant( $seach_queries ='' ){
    
        if( empty($seach_queries) )
                return FALSE;
    
        $q      =   '';
        $table  =   'participants';
        $count  =   0;
        $fulltext   = '`search_meta`';
        $display    = array();
        $result     = array(); 
        $LongEnough = 0;
        $Search     = '';
        $SearchDesc = '';
        $LookFor    = $seach_queries;
    
        // Clean up user data
        $LookFor=str_replace("AND"," ",$LookFor); 
        $LookFor=str_replace("and"," ",$LookFor); 
        $LookFor=str_replace("OR"," ",$LookFor); 
        $LookFor=str_replace("or"," ",$LookFor); 
        $LookFor=str_replace("-"," ",$LookFor); 
        $LookFor=str_replace(","," ",$LookFor); 
        $LookFor=str_replace("|"," ",$LookFor); 
        $LookFor=str_replace("'"," ",$LookFor); 
        $LookFor=str_replace('"'," ",$LookFor); 
        $LookFor=str_replace('&'," ",$LookFor); 
        $LookFor=str_replace('<'," ",$LookFor); 
        $LookFor=str_replace('>'," ",$LookFor);     
        $exploded_LookFor=explode(' ', $LookFor); 
    
        $num=( count($exploded_LookFor) - 1);

        for ($counter=0; $counter<=$num; $counter++) {

                if (strlen($exploded_LookFor[$counter])>3)
                {
                        $LongEnough=1;
                        if ($Search=="")
                                $Search = '+'.$exploded_LookFor[$counter].'*';
                        else
                                $Search .= ' +'.$exploded_LookFor[$counter].'*';
                }        
                else
                {
                        if ($SearchDesc=="") 
                                $SearchDesc =" AND ( {$fulltext} LIKE '%{$exploded_LookFor[$counter]}%')";
                        else
                                $SearchDesc.=" OR ({$fulltext} LIKE '%{$exploded_LookFor[$counter]}%')";
                }
        }

        if ($LongEnough>0){
                $q= "SELECT * FROM $table WHERE `active` = 'yes' AND match $fulltext against ('$Search' IN BOOLEAN MODE)";
        }

        if ($LongEnough!=1){
                $Search=ltrim($SearchDesc, " AND");
        }
        
        if ($q==''){
                if ($Search!=''){
                        $q = "SELECT * FROM $table WHERE `active` = 'yes' AND $Search";
                }
                else{
                        $q = "SELECT * FROM $table WHERE `active` = 'yes' $SearchDesc ";
                }
        }    

        return $q;
    
}
/**
 * Load all of the styles that need to appear on all pages
 */
function liep_theme_styles(){ 
        wp_enqueue_style( 'liep_css', get_template_directory_uri() .'/liep/assets/css/liep.css' );
}
/**
 * Load all of the scripts that need to appear on all pages
 */
function liep_theme_scripts() {
    
        $jquery_ui = array(
                "jquery",
                "jquery-ui-core",     
                "jquery-ui-widget",
                "jquery-ui-position",            
                "jquery-ui-autocomplete",
                "jquery-effects-core",     
                "jquery-effects-blind",     
                "jquery-effects-highlight",
        );
        
        foreach($jquery_ui as $script){
            wp_enqueue_script($script);
        } 
        
        wp_enqueue_script(
		'liep_lib',
		get_stylesheet_directory_uri() . '/liep/assets/js/liep.lib.js',
		$jquery_ui
        );    
        wp_enqueue_script(
		'liep_js',
		get_stylesheet_directory_uri() . '/liep/assets/js/liep.js',
		array( 'liep_lib' ) 
        );       
}
/**
 * Add a page with template
 */
function liep_add_page(){
    
        global $wpdb;
        
        $the_page_title = 'liep' ;

        $the_page = get_page_by_title( $the_page_title );
        if ( !$the_page ) {

                $liep_page = array(
                        'post_title'=> $the_page_title,
                        'post_type'=> 'page',
                        'post_name'=> sanitize_title($the_page_title), 
                        'post_content'=> '',
                        'post_status'=> 'publish',
                        'comment_status'=> 'closed',
                        'ping_status'=> 'closed',
                        'post_author'=> 1,
                        'post_category'=> array(1),
                        'menu_order'=> 0
                );
                $post_id = wp_insert_post( $liep_page, TRUE ); 
                add_post_meta($post_id, '_wp_page_template', 'page-liep.php', TRUE);                
        }
}
/**
 * Add specific CSS class by filter 
 * @param array $classes
 * @return string
 */ 
function liep_body_class_names($classes) {
        $classes[] = 'liep';
        return $classes;
}
/**
 * Load a template part into a template with parameter
 *
 *
 * Includes the named template part for a theme or if a name is specified then a
 * specialised part will be included. If the theme contains no {slug}.php file
 * then no template will be included.
 *
 * For the $name parameter, if the file is called "{slug}-special.php" then specify
 * "special".
 *
 * For the $param parameter, it should always the hash array ('key'=>'value')
 *
 * @uses extract() include() locate_template($name, FALSE, FALSE)
 *
 * @param string $slug The slug name for the generic template.
 * @param string $name The name of the specialised template.
 * @param array  $param The value need passing to the inclued file
 *
 * @return mixed Define the variable(s) of $param, with the prefix 'VIEW_', by extract() and load the {$name}.php
 */
function get_template_part_with_param( $name, $param = array('module'=>'default') ) {

        $name = (string) $name;
        if(isset($param) and count($param) > 0){
            extract($param);
            //extract($param, EXTR_PREFIX_ALL, "VIEW");
        }
        
        if( strpos($name, '.php') == FALSE ){
            
                if(is_file( TEMPLATEPATH .'/'. "{$name}.php" )){
                    include(locate_template( "{$name}.php" , FALSE, FALSE));
                }else {
                    echo "{$name}.php does not exists";
                }
        }
        else
            include $name;
}
/**
 * Handle WP query vars
 * 
 * @param array $qvars
 * @return string
 */
function liep_query_var() {
        global $wp;
        $wp->add_query_var('stm');
        $wp->add_query_var('pid');
        $wp->add_query_var('tags');
        $wp->add_query_var('years');
}
/**
 * Formart the data for displying use
 * 
 * @param type $str
 * @return type
 */
function formart_braced_data($str){
        $str = str_replace('[ ]', '', $str);
        $str = str_replace('[,]', '', $str);
        $str = str_replace('[,', '[', $str);
        $str = str_replace(',]', ']', $str);
        $str = str_replace(',]', ']', $str);
        $str = str_replace('[', '(', $str);
        $str = str_replace(']', ')', $str);
        $str = str_replace('))', ')', $str);
        
        return $str;
}
/**
 * Update clicks for LIEP 
 * @param type $data
 */
function liep_mega_count( $data ){    
        global $liep;
        $result = $liep->insert( 'action_log', $data, array( '%s', '%d', '%s' ) );
        return $result;
}
function liep_tags_log() {
        global $liep;
        
        $g_tags=get_query_var('tags');
        $g_stm = get_query_var('stm');
        
        if( $g_tags and $g_stm ){
                $tags = explode('|', $g_tags);
                foreach ($tags as $value) {
                        $data = array( 'stream_id' =>(int)$g_stm, 'tag' => $value );
                        $liep->insert( 'search_tags_log', $data, array( '%d', '%s' ) );                        
                }
        } 
}
function bus_liep_mega_count($data){
        $result = liep_mega_count( array(
                'option_name' =>'participants',
                'option_id' => $data['id'], 
                'action_type' => $data['type'] ) 
        );
        return $result;
}
#==================== Ajax Call ===============================#
/**
 * Return the include file content 
 * 
 * @param type $file
 * @return type
 */
function return_output( $file, $data ){
    ob_start();
    get_template_part_with_param($file, $data);
    return ob_get_clean();
}
/**
 * 
 * Get data on when View More button clicks
 * 
 * @param type $data
 * @return type
 */
function bus_more_participants($data){
        list($total, $result) = get_participant_by_tags($data['stm'], $data['tags'], $data['year'], $data['offset'], $data['total']);
        $content = return_output('/liep/templates/content-snap-result', array('result' => $result));
        return $content;
}

function bus_share_participant($data){
    return true;    
    //mail($to, $subject, $message, $headers);
}
/**
 * Calling the target function and return JSON like result
 * 
 * @param      Array     $post_data
 * @return      JSON Array
 */
function get_liep_data($post_data){
        
        // Define calling target function name
        $post_data['fun'] = 'bus_'.$post_data['fun'];
        
        $fun = ( isset( $post_data['fun'] ) ? $post_data['fun'] : FALSE);
        // Define calling target function parameter
        $data = ( isset( $post_data['ref'] ) ? $post_data['ref'] : FALSE);
        
        //Check if the calling target function exists
        if(function_exists( $fun ) ){
                
                try{  
                        // Call defined custom function with or without parameter               
                        $result = ($data === FALSE ? call_user_func($fun) : call_user_func($fun, $data));           
                        
                        // Return result as Json
                        echo json_encode( array('result' => $result) ); 
                        
                } catch (Exception $exc) {
                        // Return error as Json
                        echo json_encode( array('error' => $exc->getMessage() ) );    
                }
        }
        else{                  
                // No such function exists
                echo json_encode( array('service' => 'deny') );
        }

}
/**
 *    WordPress Ajax actions
 *    @require      PHP > 5.3
 *   
 */
add_action( 'wp_ajax_nopriv_get_data', function(){    
    if(isset($_POST['data'])){
        get_liep_data($_POST['data']);
    }
die();
});

add_action( 'wp_ajax_get_data', function(){    
    if(isset($_POST['data'])){
        get_liep_data($_POST['data']);
    }
die();
});
add_action('wp_head',function(){   
    echo '<script>var ajaxurl = "'.admin_url('admin-ajax.php').'";</script>';
});

/* 
< script>
// Define the WordPress Ajax url
var ajaxurl = "http://dev.learn.utoronto.ca/wp-admin/admin-ajax.php";

// Make an Ajax call to WordPress default ajax function in order to get result of selected LIEP function. eg get_all_skills_tags
$.post(  ajaxurl,
            {  action : 'get_data',
                data: { 
                                fun:  'get_all_skills_tags'
                              // ref :  1  // <- You will only need this line of code if the calling target function require a parameter
                        }
            },
            function(response){
                console.log(response);
            });
< / script>
*/