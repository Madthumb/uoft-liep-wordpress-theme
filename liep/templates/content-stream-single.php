<?php   
$stm_id = (int)get_query_var('stm');
$search_tags = get_query_var('tags');
$years = get_query_var('years');

if (!$stm_id ){
        echo '<script>window.location="'. get_permalink( get_page_by_path( LIEP_PAGE ) ).'"</script>';
        eixt();
}
$title = get_stream_title_by_id( $stm_id);
liep_mega_count( array('option_name' =>'stream', 'option_id' => $stm_id, 'action_type' => 'view' ) );
?>
<h1 class="page-title stream-title"><?php echo $title['name']; ?></h1>

<div class="panel panel-default">
    <div class="panel-heading">Find a Candidate</div>
    <div class="panel-body">‌
        <form method="get" action="">
                <dl class="dl-horizontal">
                        <dt>Experience</dt>
                        <dd class="stream-search">
                                <?php
                                        $years_level = get_years_of_experience_array();
                                        echo '<select class="year-of-experience" name="years">';
                                        echo '<option value="all">All Experience Levels</option>';
                                        foreach ($years_level as $k => $v){
                                                echo "<option value='{$k}'>{$v}</option>";
                                        }
                                        echo '</select>';
                                ?>                            
                        </dd>
                        <dt>Skills Tag</dt>
                        <dd class="stream-tags">
                                <?php
                                    $tags = get_all_skills_tags_by_stream_id($stm_id);

                                    foreach ($tags as $key=> $value){
                                            $tag = str_replace(', ', ',', $value['value']);
                                            $stream_tags[]= htmlentities(trim($tag), ENT_QUOTES);
                                    }
                                ?>
                                <ul id="skill-tags" data-tags="<?php echo implode(',', $stream_tags); ?>"></ul>
                                <input type="hidden" name="stm" value="<?php echo $stm_id; ?>">
                                <button type="submit" class="btn btn-default tag-search-btn">Search</button>
                        </dd>        
                </dl>            
        </form>
    </div>
</div>

<?php if( !empty($search_tags) ) :  ?>
<ul class="query-tags">
        <?php 
                if( strpos($search_tags, '|') == FALSE ){
                        echo '<li>'.$search_tags.'</li>';
                }else {
                        echo '<li>'. str_replace('|', '</li><li>', $search_tags).'</li>';
                }
        ?>
</ul>
<?php endif; ?>

<div class="search-result" id="search-result">
    <?php
        
        $type = 'init';
        if( !empty($search_tags)){
                $type = 'search';
                list($total, $result) = get_participant_by_tags($stm_id, $search_tags, $years);
        }else{
                $result = get_participants_by_stream_id_with_rand_limit($stm_id, $GLOBALS['records_pre_page']);
        }

        if( $result ) {
                get_template_part_with_param('/liep/templates/content-snap-result', array('result' => $result, 'type' =>$type));
        }
        else{
                echo  'There are no records available. Please try a different search tag.';
        }?>
</div>

<?php  
    if( !empty($search_tags) ) :
        
        if(count($total) > $GLOBALS['records_pre_page'] ) : ?>
                <div class="view-more">
                        <button id="load-more" class="btn btn-default view-more-btn" 
                                data-offset="<?php echo $GLOBALS['records_pre_page']; ?>"
                                data-limit ="<?php echo $GLOBALS['records_pre_page']; ?>"
                                data-total="<?php echo count($total); ?>" 
                                data-stm="<?php echo $stm_id; ?>"
                                data-tags="<?php echo $search_tags; ?>"
                                data-year="<?php echo $years; ?>"
                                type="button">
                                <span class="btn-label">View More <?php
                                        if( !empty($search_tags)){
                                                echo '<span id="result-hit">( [limit] / [total]  )</span>';
                                        }
                                ?></span>
                                <span class="spinner"></span>
                        </button>
                </div>
        <?php else: ?>
                <div class="view-more result-bg">
                    Total available candidate(s): <strong><?php echo count($total); ?></strong>
                </div>
        <?php endif; ?>
        <input type="hidden" id="myTags" value="<?php echo  $search_tags; ?>" />
<?php endif; ?>
