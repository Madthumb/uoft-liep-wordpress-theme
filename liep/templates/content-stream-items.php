<ul class="liep-stream-items">
<?php
        $streams =  get_all_streams();
        foreach ($streams as $key => $value) {
            echo '<li class="liep-stream grid_2 '.sanitize_title( $value['name'] ).'">';
            echo '<a class="" href="'.add_query_arg(array('stm' => $value['id']) ).'">';
            echo $value['name'];
            echo '</a>';
            echo '</li>';
        }
?>
</ul>