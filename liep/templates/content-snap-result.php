<?php       
        foreach ($result as $key => $value) :
            
                list($fname, $mname, $lname) = explode('|', $value['full_name']);
        
                if(strpos($value['edu_info'], '|') !== false)
                        $edu = explode('|', $value['edu_info']);
                else
                        $edu[0] = $value['edu_info'];
                
                if(strpos($value['aq_info'], '|') !== false)
                        $aq_info = explode('|', $value['aq_info']);
                else
                        $aq_info[0] = $value['aq_info'];
                
                if(strpos($value['cer_info'], '|') !== false)
                        $cer_info = explode('|', $value['cer_info']);
                else
                        $cer_info[0] = $value['cer_info'];
                
                if(strpos($value['skills'], '|') !== false)
                        $skill_ids = str_replace('|', "','", $value['skills'] );
                else
                        $skill_ids[0] = $value['skills'];       
                
                if(strpos($value['other_info'], ';') !==false)
                        $others = explode(';', $value['other_info']);
                else
                        $others[0]= $value['other_info'];
                
                
                if( $type == 'search' ){
                        liep_mega_count( array( 'option_name' =>'participants', 
                                                            'option_id' => $value['id'], 
                                                            'action_type' => $type) 
                                                );
                }
    ?>
        <div class="panel panel-default">
            <div class="panel-heading candidate">
                <div class="info">
                    <strong><?php echo $fname .($mname ? ' '.$mname.' ' : ' ' ).$lname; ?></strong> ( ID: <?php echo $value['p_id']; ?>)
                </div>
                <div class="actions">
                    <div class="btn-group">
                            <button class="btn btn-default action-btn" type="button" data-target="detail" data-id="<?php echo $value['id'];?>" >
                                    <i class="fa fa-info"></i>
                            </button>
                            <button class="btn btn-default action-btn" type="button" data-target="share" data-id="<?php echo $value['id'];?>" >
                                    <i class="fa fa-envelope"></i>
                            </button>
                            <button class="btn btn-default action-btn" type="button" data-target="print" data-id="<?php echo $value['id'];?>" >
                                    <i class="fa fa-print"></i>
                            </button>
                            <button class="btn btn-default action-btn" type="button" data-target="download" data-id="<?php echo $value['id'];?>" >
                                    <i class="fa fa-download"></i>
                            </button>
                    </div>
                </div>
                <div class="share">
                    <div class="input-group input-group-sm">
                            <input type="text" class="form-control share-email" placeholder="Enter an email to share with">
                            <span class="input-group-btn">
                                    <button type="button" class="btn btn-default share-sent" data-tid="<?php echo $value['id'];?>" >Send</button>
                            </span>
                    </div>              
                </div>
            </div>
            <div class="panel-body" role="details-<?php echo $value['id']; ?>">‌ 
                <dl class="dl-horizontal extra-info">
                    <dt>Years of Experience</dt>
                    <dd><ul><li class="no-list"><?php
                            $yr = get_years_of_experience_array();
                            echo $yr[$value['years_of_experience']]; 
                            ?>
                            </li></ul>
                    </dd>
                </dl>

                <dl class="dl-horizontal extra-info">
                    <dt>Skills</dt>
                    <dd>
                         <ul>
                        <?php 
                            $skill_tags = get_skill_tags_by_ids($skill_ids);
                            foreach ($skill_tags as $v) {
                                    foreach ($v as $sks) {
                                            echo '<li>'.formart_braced_data($sks).'</li>';
                                    }
                            }            
                        ?>
                        </ul>
                    </dd>
                </dl>            

                <dl class="dl-horizontal">
                    <dt>Education Experience</dt>
                    <dd class="stream-search">
                        <ul>
                        <?php 
                            foreach ($edu as $v) {
                                echo '<li>'.formart_braced_data($v).'</li>';
                            }
                        ?>
                        </ul>                                                    
                    </dd>      
                </dl>  

                <dl class="dl-horizontal">
                    <dt>Additional Qualifications</dt>
                    <dd class="stream-search">
                        <ul>
                        <?php 
                            foreach ($aq_info as $v) {
                                    echo '<li>'.formart_braced_data($v).'</li>';
                            }
                        ?>
                        </ul>                                                    
                    </dd>      
                </dl>  

                <dl class="dl-horizontal">
                    <dt>Certification Associations</dt>
                    <dd class="stream-search">
                        <ul>
                        <?php 
                            foreach ($cer_info as $v) {
                                    echo '<li>'.formart_braced_data($v).'</li>';
                            }
                        ?>
                        </ul>                                                    
                    </dd>      
                </dl> 

                <dl class="dl-horizontal extra-info">
                    <dt>Other Information</dt>
                    <dd><ul><?php

                        foreach ($others as $v) {
                            echo '<li>'.formart_braced_data($v).'</li>';
                        }
                            ?></ul>
                    </dd>
                </dl>

                <dl class="dl-horizontal extra-info">
                    <dt>LIEP Completion</dt>
                    <dd><ul><?php
                            echo '<li>'.date('F, Y', $value[liep_completion]) .'</li>';
                            ?></ul>
                    </dd>
                </dl>

            </div>
        </div>
    
    <?php endforeach; ?>