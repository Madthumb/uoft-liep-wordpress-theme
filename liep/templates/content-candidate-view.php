<?php
        $pid =(int)get_query_var('pid');
        $data = get_participant_by_id( $pid );
        if(empty($data)){
            die('An invalid candidate');
        }
        $data = $data[0];
    
        list($fname, $mname, $lname) = explode('|', $data['full_name']);

        if(strpos($data['edu_info'], '|') !== false)
                $edu = explode('|', $data['edu_info']);
        else
                $edu[] = $data['edu_info'];    
        
        if(strpos($data['skills'], '|') !== false)
                $skill_ids = str_replace('|', "','", $data['skills'] );
        else
                $skill_ids[] = $data['skills'];              
        
        if(strpos($data['aq_info'], '|') !== false)
                $aq_info = explode('|', $data['aq_info']);
        else
                $aq_info[] = $data['aq_info'];        
        
        if(strpos($data['cer_info'], '|') !== false)
                $cer_info = explode('|', $data['cer_info']);
        else
                $cer_info[] = $data['cer_info'];        
        
        if(strpos($data['other_info'], ';') !==false)
                $others = explode(';', $data['other_info']);
        else
                $others[]= $data['other_info'];        

?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <title>Candidate | School of Continuing Studies </title>
      <style>
        table{
                border: thin solid black;
                width: 100%;
                font-size: 13px;
        }   
        td.name{
                font-weight: bold;  
        }
        th.short{
                width: 30%;
                text-align: left;
                height: 30px;
        }
        .bold{
                font-weight: bold;
                text-align: left;
        }
        h3{
                font-size: 21px;
                font-weight: bold;
        }
        .footer{
                margin-top: 50px; 
                border: none;
                font-size: 10px;
        }   
        </style>
  </head>
  <body>
    <h3>Licensing International Engineers into the Profession (LIEP) Program</h3>
    <table>
      <tbody>
        <tr>
          <th class="short">Participant ID:</th>
          <td><?php echo $data['p_id']; ?></td>
        </tr>
        <tr>
          <th class="short">Stream:</th>
          <td><?php 
                $title = get_stream_title_by_id( (int)$data['stream_id'] ); 
                echo $title['name'];
          ?></td>
        </tr>
        <tr>
          <th class="short">Full Name:</th>
          <td class="name"><?php echo $fname .($mname ? ' '.$mname.' ' : ' ' ).$lname; ?></td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr>
          <th class="bold">Education/Degree:</th>
        </tr>
        <tr>
          <td><?php 
                foreach ($edu as $v) {
                    echo formart_braced_data($v).'<br/>';
                }
          ?></td>
        </tr>
      </tbody>
    </table>    
    <br>
    <table>
      <tbody>
        <tr>
          <th class="short">Years of Experience:</th>
          <td><?php
                $yr = get_years_of_experience_array();
                echo $yr[$data['years_of_experience']];           
          ?></td>
        </tr>
      </tbody>
    </table>    
    <br>
    <table>
      <tbody>
        <tr>
          <th class="bold">Skills and Expertise:</th>
        </tr>
        <tr>
          <td><?php 
                        $skill_tags = get_skill_tags_by_ids($skill_ids);
                        foreach ($skill_tags as $v) {
                            foreach ($v as $sks) {
                                echo formart_braced_data($sks).'<br/>';
                            }
                        }            
                ?>
          </td>
        </tr>
      </tbody>
    </table>    
    <br>
    <table>
      <tbody>
        <tr>
          <th class="bold">Additional Qualifications:</th>
        </tr>
        <tr>
          <td><?php 
                foreach ($aq_info as $v) {
                        echo formart_braced_data($v).'<br/>';
                }          
          ?></td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr>
          <th class="bold">Profession Certifications/Associations:</th>
        </tr>
        <tr>
          <td><?php 
                foreach ($cer_info as $v) {
                        echo formart_braced_data($v).'<br/>';
                }          
          ?></td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr>
          <th class="bold">Other Information:</th>
        </tr>
        <tr>
          <td><?php 
                foreach ($others as $v) {
                    echo formart_braced_data($v).'<br/>';
                }                 
          ?></td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr>
          <th class="bold">LIEP Program Completion:</th>
        </tr>
        <tr>
          <td>
            <?php 
                echo date('F Y', $data[liep_completion]); 

                if( (int)$data[liep_completion] < (int)time()  )
                    echo ' ( Successfully Completed )';
                else
                    echo ' ( Expected )';
            ?> 
          </td>
        </tr>
      </tbody>
    </table>
    <table class="footer">
      <tbody>
        <tr>
          <th class="bold">Amena Zafar, LIEP Work Experence and Mentoring Coordinator </th>
        </tr>
        <tr>
          <td>
            Phone: 416-987-5662
            <br>
            Email: scs.liep@utoronto.ca
          </td>
        </tr>
      </tbody>
    </table>
    <script>
        window.onload=function(){
                self.print();
        }
    </script>    
  </body>
</html>