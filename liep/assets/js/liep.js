/**
 * LIEP utility class
 *  
 * @author  Tony Lee<var.tonylee@gmail.com>
 * @param   jQuery $
 * @returns Object
 */
(function($) {
        var LIEPUFT = {
                liep: {
                        init: function() {
                            
                                $('#skill-tags').tagit({
                                        availableTags: $('#skill-tags').data('tags').split(','),
                                        allowSpaces: true,
                                        singleField:true,
                                        singleFieldDelimiter:'|',
                                        autocomplete: {delay: 0, minLength: 1}
                                });
                                
                                $('.action-btn').click(function(){
                                    
                                        var el = $(this).data('target'),
                                             dt =  $(this).data('id');
                                             
                                        switch(el){
                                                case 'share':
                                                            $('.share-email').val('');
                                                            $('.' + el).slideToggle("slow");
                                                        break;
                                                case 'download':
                                                        window.location='/admin/service.html?action=maker&bus=pdf&data=' + dt;
                                                        break;
                                                case 'print':
                                                        window.open('/liep/liep/?pid='+$(this).data('id'), "popupWindow", "width=600,height=600,scrollbars=yes");                
                                                        break;
                                                case 'detail':
                                                        $('div[role="details-' + dt + '"]').children('.extra-info').fadeToggle();
                                                        break;
                                        };
                                        $.post(  ajaxurl,
                                                {  action : 'get_data',
                                                    data: { 
                                                                fun:  'liep_mega_count',
                                                                ref :  {
                                                                                id : $(this).data('id'),
                                                                                type : el
                                                                        }
                                                            }
                                                },
                                                function(response){
                                                        console.log(response);
                                                }
                                        );
                                
                                });

                                if( $('#result-hit').length > 0 ){
                                        var content = $('#result-hit').html();
                                                content =  content.replace('[limit]', $('#load-more').data('limit') );
                                                content =  content.replace('[total]', $('#load-more').data('total') );
                                                $('#result-hit').html( content ).fadeIn('slow');
                                                
                                }
                                
                                $('.share-email').keyup(function(){
                                        $('.share-email').val( $(this).val() );
                                });    
                                $('.share-sent').click(function(){
                                    
                                        var data ={ email : $(this).parent().siblings('.share-email').val(),
                                                          id:  $(location).attr('protocol') + '//'
                                                              +$(location).attr('host') + '/admin/service.html?action=maker&bus=pdf&data=' 
                                                              + $(this).data('tid')
                                                        };
                                        var el = $(this);
                                        if( isEmail(data.email) ){
                                                $.post(  ajaxurl, 
                                                        {  action : 'get_data',
                                                            data: {  fun:  'share_participant',
                                                                        ref :  data
                                                                    }
                                                        },
                                                        function(response){
                                                                var jsonObj = JSON.parse(response); 
                                                                console.log(  jsonObj.result  );
                                                                if( jsonObj.result ){
                                                                        el.css({color: 'white',  'background-color':'green'}).html('Sent');
                                                                }else{    
                                                                        el.css({color: 'white',  'background-color':'red'}).html('Error');
                                                                }
                                                        });
                                        }else{
                                                $(this).css({color: 'white',  'background-color':'red'}).html('Invalid');
                                        }
                                        
                                        
                                        
                                });
                                
                                $(document).on('click','#load-more',function(){ 
                                    
                                        var data = {
                                                limit:      $(this).attr('data-limit'),
                                                offset:     $(this).attr('data-offset'),
                                                stm:        $(this).attr('data-stm'),
                                                tags:       $(this).attr('data-tags'),
                                                total:      $(this).attr('data-total'),
                                                year:       $(this).attr('data-year')
                                        };
                                        
                                        if( data.offset > data.total){
                                                data.offset = data.total;
                                        }
                                        
                                        $.post(  ajaxurl, 
                                                {  action : 'get_data',
                                                    data: { fun:  'more_participants',
                                                               ref :  data
                                                            }
                                                },
                                                function(response){
                                                    
                                                        // Get value of attributes                                                        
                                                        var el = $('#load-more'),
                                                              total = el.attr('data-total'),
                                                              offset =  el.attr('data-offset'),
                                                              limit = el.attr('data-limit');
                                                        
                                                        // Get return result
                                                        var jsonObj = JSON.parse(response);           
                                                        
                                                        $('.search-result').append(jsonObj.result);
                                                        
                                                        // Reset button attributes
                                                        var newOffset =  parseInt(offset) + parseInt(limit);
                                                        
                                                        if(newOffset >= total ){    
                                                                $('.view-more').addClass('result-bg')
                                                                                     .html('Total available candidate(s): <strong>' + total +'</strong>' );
                                                        }else{
                                                                var loadedItem = newOffset;
                                                                el.attr('data-offset', newOffset );
                                                                $('#result-hit').html('( '+loadedItem+' / '+total+'  )');
                                                        }
                                                }
                                        );
                                        
                                });
                                
                                
                                if( $('#myTags').length > 0 ){
                                        var myTags = new highlightTags("search-result"); 
                                        myTags.apply(  $('#myTags').val()  ); 
                                }
                                        
                                
                        }
                }
        };
        var LIEP_UTIL = {
                fire: function(func, funcname, args) {
                        var namespace = LIEPUFT;
                        funcname = (funcname === undefined) ? 'init' : funcname;
                        if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
                                namespace[func][funcname](args);
                        }
                },
                loadEvents: function() {
                        $.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm) {
                                LIEP_UTIL.fire(classnm);
                        });
                }
        };
        function isEmail(email) {
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                return regex.test(email);
        }
$(document).ready(LIEP_UTIL.loadEvents);})(jQuery);