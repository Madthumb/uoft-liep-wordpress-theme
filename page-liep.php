<?php 
/* 
Template Name: LIEP Template
*/

if(!get_query_var('pid')){
    
        global $post; 
        
        get_header();
        
        $single = FALSE;
        $router = 'stream-items';                                

        if(get_query_var('stm') ){
                $router = 'stream-single';
                $single = TRUE;
                
                liep_tags_log();
        }
        ?>
        <section class="grid_9 noleft <?php echo $router; ?>">
                <div class="grid_2 noleft in_section" style="background-color: #e4e4e4;">.</div>
                <section class="content grid_7">
                        <article>                    
                                <?php 
                                        if($single === FALSE ){
                                                while ( have_posts() ) : the_post();
                                                        the_title( '<h1 class="page-title">', '</h1>' );
                                                        the_content();
                                                endwhile;
                                        }                                
                                        get_template_part( 'liep/templates/content', $router);
                                ?>                        
                        </article>
                </section>    

        <?php
        get_sidebar();
        get_footer();
}
else{
        get_template_part( 'liep/templates/content-candidate', 'view');
}